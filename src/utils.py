#Contains different general purpose methods 



def print_sudoku(mat):
    return [print(mat[i]) for i in range(0, len(mat))]


def matrix_gen(size):
    return [[0]*size for i in range(0, size)]

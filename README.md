# Project - Sudoku

## Requirements

To install project dependencies, in project directory run:

```bash
pip install -r requirements.txt
```

## Project Structure

In the root directory, you will find:

- **src/**: contains source code
- **inputs/**: contains input files
- **src/main.py**: main entry point
- **src/generator.py**: contains code for generating Sudokus
- **src/solver.py**: contains code for our sat solver
- **src/utils.py**: contains general use code
- **src/z3_solver.py**: contains code that uses z3 solver


## Miscellaneous commands

Here are a few useful commands you can use from src directory.

To run using an input file:

```bash
python3 main.py -i ../inputs/input_<number>.txt --ss <solver 1 or 2>
```

To run using a sudoku generator:
```bash
python3 main.py -s <dimension> -l <level of difficulty from 1 to 3> --ss <solver 1 or 2>
```


## Versions used

Python version:
`3.9.2`


from z3 import *
from math import sqrt
from random import *
import argparse
from utils import *
from solver import *
from generator import *
from z3_solver import *


def main():
    parser = argparse.ArgumentParser(description='Sudoku')
    parser.add_argument("-ss", "--solver", type=int, choices=range(1, 3), help="Choose solver; 1 for our solver, 2 for z3 solver", default=1)
    parser.add_argument("-s", "--size", type=int, choices=range(1, 50), help="Choose row and column size", default=9)
    parser.add_argument("-l", "--level", type=int, choices=range(1, 4), help="Choose difficulty level", default=1)
    parser.add_argument("-i", "--input", help="Choose input file")

    options = parser.parse_args()
        
    if options.input:
        input_file = open(options.input, "r")
        size =  int(input_file.readline())
        sudoku_mat = [list(map(int, l.split(","))) for l in input_file.readlines()]
    else:
        size = options.size
        level = options.level

        if sqrt(size)!=int(sqrt(size)):
            print("Need to be a number that has whole square root")
            return 1

        if level == 1:
            print("Easy level")
            sudoku_mat = gen_easy_sudoku(size)
        elif level == 2:
            print("Medium level")
            sudoku_mat = gen_medium_sudoku(size)
        else:
            print("Hard level")
            sudoku_mat = gen_hard_sudoku(size)


    print_sudoku(sudoku_mat)

    if not valid_sudoku(sudoku_mat, size):
        print("Unsat")
        return 0

    if options.solver == 2:
        mat = z3_solver(sudoku_mat, size)
    else:
        mat, satisfiable = solve_sudoku(sudoku_mat, size)
    
    print_sudoku(mat)


if __name__ == '__main__':
	main()


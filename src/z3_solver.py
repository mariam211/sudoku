from z3 import *
from math import sqrt


def z3_solver(instance, size):
    sroot = int(sqrt(size))

    X = [ [ Int("x_%s_%s" % (i+1, j+1)) for j in range(size) ] for i in range(size) ]

    # each cell contains a value in {1, ..., size}
    cells_c  = [ And(1 <= X[i][j], X[i][j] <= size) for i in range(size) for j in range(size) ]

    # each row contains a digit at most once
    rows_c   = [ Distinct(X[i]) for i in range(size) ]

    # each column contains a digit at most once
    cols_c   = [ Distinct([ X[i][j] for i in range(size) ]) for j in range(size) ]

    # each sroot x sroot square contains a digit at most once
    sq_c     = [ Distinct([ X[sroot*i0 + i][sroot*j0 + j] for i in range(sroot) for j in range(sroot) ]) for i0 in range(sroot) for j0 in range(sroot) ]

    sudoku_c = cells_c + rows_c + cols_c + sq_c

    instance_c = [ If(instance[i][j] == 0, True, X[i][j] == instance[i][j]) for i in range(size) for j in range(size) ]

    s = Solver()
    s.add(sudoku_c + instance_c)
    r = instance

    if s.check() == sat:
        print("sat")
        m = s.model()
        r = [ [ m.evaluate(X[i][j]) for j in range(size) ] for i in range(size) ]
    else:
        print("failed to solve")

    return r

from z3 import *
from math import sqrt
from random import *
import argparse
from utils import *

MAX_BACKTRACK=10000


def min_solution(mat, size):
    """
    Finds cell with minimum number of possible values(with max constraints)
    """
    position = (-1, -1)
    minimum = size+1
    min_vals = []

    for i in range(0, size):
        for j in range(0, size):
            if(mat[i][j]==0):
                nb_vals, vals, _ = possible_values(mat, i, j, size)
                if(nb_vals<minimum):
                    minimum = nb_vals
                    min_vals = vals
                    position = (i, j)

    return minimum, min_vals, position


def max_solution(mat, size):
    """
    Finds cell with maximum number of possible values(with minimum constraints)
    """
    position = (-1, -1)
    maximum = -1
    max_vals = []

    for i in range(0, size):
        for j in range(0, size):
            if(mat[i][j]==0):
                nb_vals, vals, _ = possible_values(mat, i, j, size)

                if(maximum<nb_vals):
                    maximum = nb_vals
                    max_vals = vals
                    position = (i, j)

    return maximum, max_vals, position


def possible_values(mat, i, j, size):
    """
    Gives possible values of a given cell
    """
    sroot = int(sqrt(size))
    row_neighbors = []
    col_neighbors = []
    square_neighbors = []
    valid = True

    #collect row neighbors
    for k in range(0, size):
        if(mat[k][j] in row_neighbors):
            valid = False
        if(k!=i and mat[k][j]!=0):
            row_neighbors.append(mat[k][j])

    #collect column neighbors
    for l in range(0, size):
        if(mat[i][l] in col_neighbors):
            valid = False
        if(l!=j and mat[i][l]!=0):
            col_neighbors.append(mat[i][l])

    square = False
    #collect neighbors in the sroot*sroot square
    #last squares starts at column index size-sroot
    for k in range(size-sroot,-1,-sroot):
        if(square):
            break #to avoid including current position in other squares
        for l in range(size-sroot,-1,-sroot):
            #min in last squares is j=size-sroot and i=0
            if(i>=k and j>=l):
                for r in range(k, k+sroot):
                    for c in range(l, l+sroot):
                        if(mat[r][c] in square_neighbors):
                            valid = False   #found similar values
                        if((i!=r or j!=c) and mat[r][c]!=0):
                            square_neighbors.append(mat[r][c])
                
                square=True
                break

    neighbors = set(row_neighbors + col_neighbors + square_neighbors)
    pos_values = list(set(range(1,size+1)) - neighbors)
    nb_values = len(pos_values)

    #to get random order of values
    shuffle(pos_values)

    return nb_values, pos_values, valid


def back_track(decision_level, decisions, mat, filled_cells=0, backtrackings=0):
    """
    Used to backtrack decisions
    """
    if decision_level<0 or backtrackings>MAX_BACKTRACK:
        return decision_level, decisions, mat, filled_cells, backtrackings
    
    backtrackings += 1
    #erase last decision implications
    for position in decisions[decision_level]["implications"]:
        mat[position[0]][position[1]] = 0
        filled_cells -= 1

    decisions[decision_level]["implications"] = {}
    remaining_values = decisions[decision_level]["decision"]["remaining_values"]
    nb_remaining = len(remaining_values)
    position = decisions[decision_level]["decision"]["position"]

    #choose the other remaining value if it exists else backtrack
    if nb_remaining>0:
        new_value = remaining_values.pop()
        mat[position[0]][position[1]] = new_value
        filled_cells += 1
        decisions[decision_level]["decision"]["value"] = new_value
        decisions[decision_level]["decision"]["remaining_values"] = remaining_values
    else:
        #only for decision level 0 position is None
        if position:
            mat[position[0]][position[1]] = 0
            filled_cells -= 1

        decisions.pop(decision_level)
        decision_level -= 1
        decision_level, decisions, mat, filled_cells, backtrackings = back_track(decision_level, decisions, mat, filled_cells, backtrackings)

    return decision_level, decisions, mat, filled_cells, backtrackings


def brute_force(mat, decision_level, decisions, nb_vals, vals, position, satisfiable=False, filled_cells=0, backtrackings=0):
    """
    Tries possible values of a given cell
    """
    if position == (-1, -1):
        #solved!
        satisfiable = True

    elif nb_vals==0:
        #no possible value so backtrack
        decision_level, decisions, mat, filled_cells, backtrackings = back_track(decision_level, decisions, mat, filled_cells, backtrackings)

    elif nb_vals>1:
        #multiple possible values so take new decision of choosing a value
        mat[position[0]][position[1]] = vals.pop()
        decision_level += 1
        filled_cells += 1
        decisions[decision_level] = {"decision": {"position": position, "value": mat[position[0]][position[1]], "remaining_values": vals}, "implications": {}}
    
    elif nb_vals==1:
        #propagation of the only value possible
        mat[position[0]][position[1]] = vals.pop()
        filled_cells += 1
        decisions[decision_level]["implications"][position] = mat[position[0]][position[1]]

    return mat, decision_level, decisions, satisfiable, filled_cells, backtrackings


def decide(mat, size, decisions, decision_level, satisfiable=False):
    """
    Brute forces different cell values until we reach a decision; a sat or unsat
    """
    filled_cells = 0
    backtrackings = 0

    while not satisfiable and decision_level>=0 and backtrackings<=MAX_BACKTRACK:
        nb_vals, vals, position = min_solution(mat, size)
        mat, decision_level, decisions, satisfiable, filled_cells, backtrackings = brute_force(mat, decision_level, decisions, nb_vals, vals, position, satisfiable, filled_cells, backtrackings)
    
    # print(decisions)
    # print("filled cells", filled_cells)

    return mat, decision_level, decisions, satisfiable, backtrackings


def solve_sudoku(mat, size):
    satisfiable = False
    decision_level = 0
    decisions = {}
    decisions[decision_level] = {"decision": {"position": None, "value": None, "remaining_values": []}, "implications": {}}

    mat, decision_level, decisions, satisfiable, backtrackings = decide(mat, size, decisions, decision_level, satisfiable)

    print("Last decision level", decision_level)
    print("Number of backtrackings", backtrackings)

    if satisfiable:
        print("Sat")
        if(unique_solution(mat, decisions, decision_level, size)):
            print("Unique")
        else:
            print("Not unique")
    else:
        print("Unsat")

    return mat, satisfiable


def valid_sudoku(mat, size):
    """
    Used to check if a matrix is a valid sudoku
    """
    for i in range(0, size):
        for j in range(0, size):
            if(mat[i][j]!=0):
                _,_, valid = possible_values(mat, i, j, size)
                if(not valid):
                    return valid

    return True


def unique_solution(mat, decisions, last_level, size):
    """
    Checks unicity of solution
    """
    #most cases fall in the first condition in which last filled cell still has other possible values
    if(len(decisions[last_level]["decision"]["remaining_values"])>0):
        return False
    else:
        decision_level, decisions, mat, _, _ = back_track(last_level, decisions, mat)
        _, _, _, satisfiable, _  = decide(mat, size, decisions, decision_level, False)

    return not satisfiable

from z3 import *
from math import sqrt
from random import *
from solver import brute_force, max_solution, min_solution
from utils import *


def gen_easy_sudoku(size):
    decision_level = 0
    decisions = {}
    decisions[decision_level] = {"decision": {"position": None, "value": None, "remaining_values": []}, "implications": {}}
    mat = matrix_gen(size)
    filled_cells = 1
    coverage = int(size*size*0.2)

    #fill 20% of matrix
    while(filled_cells<=coverage):
        nb_vals, vals, position = min_solution(mat, size)
        mat, decision_level, decisions, _, filled_cells, _ = brute_force(mat, decision_level, decisions, nb_vals, vals, position, False, filled_cells)

    return mat


def gen_hard_sudoku(size):
    decision_level = 0
    decisions = {}
    decisions[decision_level] = {"decision": {"position": None, "value": None, "remaining_values": []}, "implications": {}}
    mat = matrix_gen(size)
    filled_cells = 1
    coverage = int(size*size*0.2)

    while(filled_cells<=coverage):
        nb_vals, vals, position = max_solution(mat, size)
        mat, decision_level, decisions, _, filled_cells, _ = brute_force(mat, decision_level, decisions, nb_vals, vals, position, False, filled_cells)

    return mat


def gen_medium_sudoku(size):
    decision_level = 0
    decisions = {}
    decisions[decision_level] = {"decision": {"position": None, "value": None, "remaining_values": []}, "implications": {}}
    mat = matrix_gen(size)
    filled_cells = 1
    coverage = int(size*size*0.2)

    #we start by min first since if we do the oposite we will get a result close to easy level
    while(filled_cells<=int(coverage/2)):
        nb_vals, vals, position = min_solution(mat, size)
        mat, decision_level, decisions, _, filled_cells, _ = brute_force(mat, decision_level, decisions, nb_vals, vals, position, False, filled_cells)
    
    decision_level = 0
    decisions = {}
    decisions[decision_level] = {"decision": {"position": None, "value": None, "remaining_values": []}, "implications": {}}

    while(filled_cells<=coverage):
        nb_vals, vals, position = max_solution(mat, size)
        mat, decision_level, decisions, _, filled_cells, _ = brute_force(mat, decision_level, decisions, nb_vals, vals, position, False, filled_cells)

    return mat

